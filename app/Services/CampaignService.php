<?php

namespace App\Services;

use App\Http\Requests\ApiRequest;
use App\Http\Requests\CampaignRequest;
use App\Models\Campaign;
use App\Repositories\CampaignRepository;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class CampaignService
 * @package App\Service
 */
class CampaignService
{
    /**
     * @var CampaignRepository
     */
    private CampaignRepository $repository;

    public function __construct(CampaignRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getList(): array
    {
        $status = null;
        try {
            $campaigns = $this->repository->getAll();

        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Get Campaign List Successfully', 'data' => $campaigns, 'httpCode' => Response::HTTP_OK];
    }

    public function show(int $campaignId): array
    {
        try {
            $campaign = $this->repository->getShow($campaignId);
            if ($campaign === null) {
                return ['message' => 'Not found campaign', 'data' => null, 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }

        } catch (\Exception $e) {

            return ['message' => $e->getMessage(), 'data' => null, 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Get Campaign List Successfully', 'data' => $campaign, 'httpCode' => Response::HTTP_OK];
    }

    public function store(CampaignRequest $request): array
    {
        try {
            $campaign = new Campaign([
                'name' => $request->name,
                'campaign_start' => $request->campaign_start,
                'campaign_end' => $request->campaign_end,
                'status_id' => 1
            ]);

            $this->repository->persist($campaign);

        } catch (\Exception $e) {

            return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Get Campaign List Successfully', 'data' => $campaign, 'httpCode' => Response::HTTP_OK];
    }

    public function update(ApiRequest $request, int $campaignId): array
    {
        $campaign = $this->repository->getShow($campaignId);

        if ($campaign) {
            try {
                $name = !is_null($request->name) ? $request->name : $campaign->name;
                $campaign_start = !is_null($request->campaign_start) ? $request->campaign_start : $campaign->campaign_start;
                $campaign_end = !is_null($request->campaign_end) ? $request->campaign_end : $campaign->campaign_end;
                $status_id = !is_null($request->status_id) ? $request->status_id : $campaign->status_id;

                $campaign->name = $name;
                $campaign->campaign_start = $campaign_start;
                $campaign->campaign_end = $campaign_end;
                $campaign->status_id = $status_id;

                $this->repository->persist($campaign);

            } catch (\Exception $e) {
                return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }

        } else {
            return ['message' => 'Error campaign update', 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Campaign Updated Successfully', 'data' => $campaign, 'httpCode' => Response::HTTP_OK];
    }

    public function destroy(int $campaignId): array
    {
        $campaign = $this->repository->getShow($campaignId);

        if ($campaign) {
            try {
                $this->repository->destroy($campaign);
            } catch (\Exception $e) {
                return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }
        } else {
            return ['message' => 'Error campaign update', 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Campaign Delete Successfully', 'data' => $campaign, 'httpCode' => Response::HTTP_OK];
    }
}
