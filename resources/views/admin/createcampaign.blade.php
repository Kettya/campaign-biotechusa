@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">* Name:</label>
                        <input type="text" class="form-control" id="name" name="name" required/>
                    </div>
                    <div class="form-group">
                        <label for="campaign_start"> Campaign Start:</label>
                        <input type="date" class="form-control" id="campaign_start" name="campaign_start"
                               required/>
                    </div>
                    <div class="form-group">
                        <label for="campaign_end">* Campaign End: </label>
                        <input type="date" class="form-control" id="campaign_end" name="campaign_end"
                               required/>
                    </div>
{{--                    <input type="text" class="date" id="datepicker">--}}
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <input type="submit" value="Save" class="btn btn-primary mb-2"/>
                        <a href="{{redirect()->back()}}" class="btn btn-secondary mb-2">Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        // var array = ["06-11-2021","02-12-2021","23-11-2021","17-11-2021"];
        $('#datepicker').datepicker({
            // beforeShowDay: function(date){
            //     var string = jQuery.datepicker.formatDate('dd-mm-yy', date);
            //     return [ array.indexOf(string) === -1 ]
            // }
            beforeShowDay: $.datepicker.noWeekends
        });

    </script>
@endsection
