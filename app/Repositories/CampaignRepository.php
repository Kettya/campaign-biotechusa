<?php

namespace App\Repositories;

use App\Models\Campaign;

/**
 * Class campaignRepository
 * @package App\Repository
 */
class CampaignRepository
{
    /**
     * @var Campaign
     */
    private Campaign $model;

    /**
     * CampaignRepository constructor.
     * @param Campaign $model
     */
    public function __construct(Campaign $model)
    {
        $this->model = $model;
    }

    public function getAll(): array
    {
        $campaigns = $this->model->get()->all();

        return $campaigns;
    }

    /**
     * @param $campaignId
     * @return object
     */
    public function getShow($campaignId): object
    {
        return $this->model->query()->findOrFail($campaignId);
    }

    /**
     * @param object $data
     * @return campaign
     * @throws \Exception
     */
    public function persist(object $data): object
    {
        try {
            $data->save();

            return $data;

        } catch (\Exception $e) {

            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param object $campaign
     * @return int|null
     * @throws \Exception
     */
    public function destroy(object $campaign): int
    {
        try {
            return $campaign->delete();

        } catch (\Exception $e) {

            throw new \Exception($e->getMessage());
        }
    }
}
