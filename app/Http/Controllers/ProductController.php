<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /**
     * @var ProductService
     */
    private ProductService $service;

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $result = $this->service->getList();

        return response($result['data'], $result['httpCode']);
    }

    public function createProduct(ProductRequest $request, $campaigId = null)
    {

        $result = $this->service->store($request);

        return response($result['data'], $result['httpCode']);
    }

//    public function getProductCampaign($productId, $campaignId)
//    {
//        $product = Product::query()->where('id', $productId)->where('campaign_id', $campaignId)->first();
//        return $product;
//
//    }

    public function getProduct($productId)
    {
        $result = $this->service->show($productId);

        return response($result['data'], $result['httpCode']);

    }

//    public function updateProductCampaign(Request $request, $productId, $campaignId)
//    {
//        $request->validate([
//            'name' => ['required', 'unique:products'],
//            'publication_date' => ['required', 'date']
//        ]);
//        $product = Product::query()->where('id', $productId)->where('campaign_id', $campaignId)->first();
//        $name = !empty($request->name) ? $request->name : $product->name;
//        $publication_date = !empty($request->publication_date) ? $request->publication_date : $product->publication_date;
//        $product->update([
//            'name' => $name,
//            'publication_date' => $publication_date
//        ]);
//
//        return $product;
//    }

    public function updateProduct(ProductRequest $request, $productId)
    {
        $result = $this->service->update($request, $productId);

        return response($result['data'], $result['httpCode']);
    }

//    public function deleteFromCampaign($campaignId, $productId)
//    {
//        $product = Product::query()->where('id', $productId)->where('campaign_id', $campaignId)->first();
//        $product->update([
//            'campaign_id' => null
//        ]);
//        return $product;
//    }

    public function delete($productId)
    {
        $product = $this->service->destroy($productId);

        return response($product);
    }
}
