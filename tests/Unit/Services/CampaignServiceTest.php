<?php

namespace Tests\Unit\Services;

use App\Http\Requests\CampaignRequest;
use App\Models\Campaign;
use App\Repositories\CampaignRepository;
use App\Services\CampaignService;
use Carbon\Carbon;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Tests\TestCase;

class CampaignServiceTest extends TestCase
{
    private int $id;
    private array $data;

    private CampaignRepository $campaignRepository;
    /**
     * @var campaign
     */
    private campaign $model;

    protected function setUp(): void
    {
        parent::setUp();

        $this->id = 1;
        $this->data = [
            'name' => 'Test Campaign' . rand(1, 10000),
            'campaign_start' => '2021-11-07',
            'campaign_end' => Carbon::tomorrow(),
            'status_id' => 3
        ];

        $this->model = new campaign();
        $this->campaignRepository = new campaignRepository($this->model);
    }

    /**
     * @group service-campaigns
     */
    public function testGetAction()
    {
        $campaignsService = new \App\Services\CampaignService($this->campaignRepository);

        $result = $campaignsService->getList();

        $this->assertEquals(200, $result['httpCode']);
    }

    /**
     * @group service-campaigns
     */

    public function testIdAction(): void
    {
        $campaignsService = new \App\Services\CampaignService($this->campaignRepository);

        $result = $campaignsService->show($this->id);

        $this->assertEquals(200, $result['httpCode']);
    }

    /**
     * @group service-campaigns
    */
    public function testAddSuccessAction(): void
    {
        $campaignsService = new \App\Services\campaignService($this->campaignRepository);

        $result = $campaignsService->store(new CampaignRequest($this->data));

        $this->assertEquals(200, $result['httpCode']);
    }

    /**
     * @group service-campaigns
     */
    public function testUpdateSuccessAction(): void
    {
        $campaignsService = new \App\Services\campaignService($this->campaignRepository);

        $result = $campaignsService->update(new CampaignRequest($this->data), $this->id);

        $this->assertEquals(200, $result['httpCode']);
    }
}
