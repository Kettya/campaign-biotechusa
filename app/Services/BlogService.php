<?php

namespace App\Services;

use App\Http\Requests\BlogRequest;
use App\Models\Blog;
use App\Repositories\BlogRepository;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BlogService
 * @package App\Services
 */
class BlogService
{
    /**
     * @var BlogRepository
     */
    private BlogRepository $repository;

    public function __construct(BlogRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getList(): array
    {
        try {
            $blogs = $this->repository->getAll();

        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Get Blog List Successfully', 'data' => $blogs, 'httpCode' => Response::HTTP_OK];
    }

    public function show(int $blogId): array
    {
        try {
            $blog = $this->repository->getShow($blogId);
            if ($blog === null) {
                return ['message' => 'Not found blog', 'data' => null, 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }

        } catch (\Exception $e) {

            return ['message' => $e->getMessage(), 'data' => null, 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Get Blog Successfully', 'data' => $blog, 'httpCode' => Response::HTTP_OK];
    }

    public function store(BlogRequest $request, int $campaignId = null): array
    {
        try {
            $campaign = !empty($campaignId) ? $campaignId : null;
            $blog = new Blog([
                'title' => $request->title,
                'contents' => $request->contents,
                'publication_date' => $request->publication_date,
                'campaign_id' => $campaign
            ]);

            $this->repository->persist($blog);

        } catch (\Exception $e) {

            return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Blog Created Successfully', 'data' => $blog, 'httpCode' => Response::HTTP_OK];
    }

    public function update(BlogRequest $request, int $blogId): array
    {
        $blog = $this->repository->getShow($blogId);

        if ($blog) {
            try {
                $campaign = !is_null($request->campaign_id) ? $request->campaign_id : $blog->campaign_id;
                $title = !is_null($request->title) ? $request->title : $blog->title;
                $contents = !is_null($request->contents) ? $request->contents : $blog->contents;
                $publication_date = !is_null($request->publication_date) ? $request->publication_date : $blog->publication_date;

                $blog->title = $title;
                $blog->contents = $contents;
                $blog->publication_date = $publication_date;
                $blog->campaign_id = $campaign;

                $this->repository->persist($blog);

            } catch (\Exception $e) {
                return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }

        } else {
            return ['message' => 'Error blog update', 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Blog Updated Successfully', 'data' => $blog, 'httpCode' => Response::HTTP_OK];
    }

    public function destroy(int $blogId): array
    {
        $blog = $this->repository->getShow($blogId);

        if ($blog) {
            try {
                $this->repository->destroy($blog);
            } catch (\Exception $e) {
                return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }
        } else {
            return ['message' => 'Error blog update', 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Blog Delete Successfully', 'data' => $blog, 'httpCode' => Response::HTTP_OK];
    }

}
