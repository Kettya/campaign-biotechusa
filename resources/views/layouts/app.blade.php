<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Title</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}?v={{ time() }}" rel="stylesheet">
    <link href="http://code.jquery.com/ui/1.9.2/themes/smoothness/jquery-ui.css" rel="stylesheet" />
    <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
{{--    <script defer src="{{ asset('fontawesome/fontawesome.css') }}"></script>--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li><a class="nav-link" href="{{url('/admin/campaignlist')}}">Campaigns</a></li>
                    @auth
                        <li><a class="nav-link" href="#">B</a></li>
                        <li><a class="nav-link" href="#">A</a></li>
                    @endauth
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
{{--                    @guest--}}
{{--                        <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>--}}
{{--                        <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>--}}
{{--                    @endguest--}}
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @if(session()->has('message'))
            <div class="container">
                <div class="alert alert-success" role="alert">{{ session('message') }}</div>
            </div>
        @endif
        @if(session()->has('errorMessage'))
            <div class="container">
                <div class="alert alert-danger data-dismissible" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('errorMessage') }}</div>
            </div>
        @endif
        @yield('content')
    </main>
</div>

<!-- Scripts -->
{{--<script src="{{ asset('js/js.js') }}"></script>--}}
{{--<script src="{{ asset('js/app.js') }}"></script>--}}
@yield('script')
</body>
</html>
