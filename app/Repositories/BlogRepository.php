<?php

namespace App\Repositories;

use App\Models\Blog;

class BlogRepository
{
    /**
     * @var Blog
     */
    private Blog $model;

    public function __construct(Blog $model)
    {
        $this->model = $model;
    }

    public function getAll(): array
    {
        $blogs = $this->model->get()->all();

        return $blogs;
    }

    /**
     * @param $blogId
     * @return object
     */
    public function getShow($blogId): object
    {
        return $this->model->query()->findOrFail($blogId);
    }

    /**
     * @param object $data
     * @return object
     * @throws \Exception
     */
    public function persist(object $data): object
    {
        try {
            $data->save();

            return $data;

        } catch (\Exception $e) {

            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param object $blog
     * @return int
     * @throws \Exception
     */
    public function destroy(object $blog): int
    {
        try {
            return $blog->delete();

        } catch (\Exception $e) {

            throw new \Exception($e->getMessage());
        }
    }

}
