<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class FirstOrLastTreeDay implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ((new Carbon($value)) > (new Carbon($value))->startOfMonth()->addDay(2) && (new Carbon($value)) < (new Carbon($value))->endOfMonth()->subDay(3)  ) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The activation day must be in the first or last 3 days of a month!';
    }
}
