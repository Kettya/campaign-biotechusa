<?php

namespace App\Http\Controllers;

use App\Http\Requests\CampaignRequest;
use Exception;
use Illuminate\Http\Request;
use App\Models\Campaign;
use App\Services\CampaignService;

class CampaignController extends Controller
{
    /**
     * @var CampaignService
     */
    private CampaignService $service;

    public function __construct(CampaignService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $result = $this->service->getList();

        return response($result['data'], $result['httpCode']);
    }

    public function createCampaign(CampaignRequest $request)
    {
        $result = $this->service->store($request);

        return response($result['data'], $result['httpCode']);
    }

    public function getCampaign($campaignId)
    {
        $result = $this->service->show($campaignId);

        return response($result['data'], $result['httpCode']);
    }

    public function updateCampaign(CampaignRequest $request, $campaignId)
    {
        $result = $this->service->update($request, $campaignId);

        return response($result['data'], $result['httpCode']);
    }

    public function deleteCampaign($campaignId)
    {
        $campaign = $this->service->destroy($campaignId);

        return response($campaign);
    }

    public function getApproved()
    {
        $campaign = Campaign::query()->where('status_id', 3)->get();
        return $campaign;
    }
}
