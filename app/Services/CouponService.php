<?php

namespace App\Services;

use App\Http\Requests\ApiRequest;
use App\Http\Requests\CouponRequest;
use App\Models\Coupon;
use App\Repositories\CouponRepository;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class CouponService
 * @package App\Services
 */
class CouponService
{
    /**
     * @var CouponRepository
     */
    private CouponRepository $repository;

    public function __construct(CouponRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getList(): array
    {
        try {
            $coupons = $this->repository->getAll();
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }
        return ['message' => 'Get Coupon List Successfully', 'data' => $coupons, 'httpCode' => Response::HTTP_OK];
    }

    public function show(int $couponId): array
    {
        try {
            $coupon = $this->repository->getShow($couponId);
            if ($coupon === null) {
                return ['message' => 'Not found coupon', 'data' => null, 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }
        return ['message' => 'Get Coupon Successfully', 'data' => $coupon, 'httpCode' => Response::HTTP_OK];
    }

    public function store(CouponRequest $request, int $campaignId = null): array
    {
        try {
            $campaign = !empty($campaignId) ? $campaignId : null;
            $coupon = new Coupon([
                'name' => $request->name,
                'code' => $request->code,
                'activation_date' => $request->activation_date,
                'campaign_id' => $campaign
            ]);
            $this->repository->persist($coupon);
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }
        return ['message' => 'Create Coupon Successfully', 'data' => $coupon, 'httpCode' => Response::HTTP_OK];
    }

    public function update(CouponRequest $request, int $couponId): array
    {
        $coupon = $this->repository->getShow($couponId);

        if ($coupon) {
            try {
                $campaign = !is_null($request->campaign_id) ? $request->campaign_id : $coupon->campaign_id;
                $name = !is_null($request->name) ? $request->name : $coupon->name;
                $code = !is_null($request->code) ? $request->code : $coupon->code;
                $activation_date = !is_null($request->activation_date) ? $request->activation_date : $coupon->activation_date;

                $coupon->name = $name;
                $coupon->code = $code;
                $coupon->activation_date = $activation_date;
                $coupon->campaign_id = $campaign;

                $this->repository->persist($coupon);

            } catch (\Exception $e) {
                return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }
        } else {
            return ['message' => 'Error coupon update', 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }
        return ['message' => 'Coupon Updated Successfully', 'data' => $coupon, 'httpCode' => Response::HTTP_OK];
    }

    public function destroy(int $couponId): array
    {
        $coupon = $this->repository->getShow($couponId);

        if ($coupon) {
            try {
                $this->repository->destroy($coupon);
            } catch (\Exception $e) {
                return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }
        } else {
            return ['message' => 'Error coupon update', 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }
        return ['message' => 'Coupon Delete Successfully', 'data' => $coupon, 'httpCode' => Response::HTTP_OK];
    }

}
