<?php

namespace App\Http\Requests;

use App\Rules\FirstOrLastTreeDay;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'unique:coupons'],
            'code' => ['required', 'unique:coupons'],
            'activation_date' => ['required', 'date', new FirstOrLastTreeDay]
        ];
    }
}
