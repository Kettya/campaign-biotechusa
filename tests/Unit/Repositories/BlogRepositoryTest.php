<?php

namespace Repositories;

use App\Models\Blog;
use App\Repositories\BlogRepository;
use Tests\TestCase;

class BlogRepositoryTest extends TestCase
{
    private int $id;
    private array $data;

    /**
     * @var blog
     */
    private blog $model;

    protected function setUp(): void
    {
        parent::setUp();

        $this->id = 1;
        $this->data = [
            'title' => 'Test Blog' . rand(1, 10000),
            'contents' => 'Test Contents',
            'publication_date' => '2021-12-01'
        ];

        $this->model = new blog();
    }

    /**
     * @group Repository-blog
     */
    public function testGetAllAction()
    {
        $blogRepository = new \App\Repositories\BlogRepository($this->model);

        $result = $blogRepository->getAll();

        $this->assertNotNull($result);
    }

    /**
     * @group Repository-blog
     */

    public function testIdAction(): void
    {
        $blogRepository = new \App\Repositories\BlogRepository($this->model);

        $result = $blogRepository->getShow($this->id);

        $this->assertNotNull($result);
    }

    /**
     * @group Repository-blog
     */
    public function testAddSuccessAction(): void
    {
        $blogRepository = new \App\Repositories\BlogRepository($this->model);

        $result = $blogRepository->persist(new Blog($this->data));

        $this->assertNotNull($result);
    }

}
