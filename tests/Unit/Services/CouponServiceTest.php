<?php

namespace Tests\Unit\Services;

use App\Http\Requests\CampaignRequest;
use App\Http\Requests\CouponRequest;
use App\Models\Coupon;
use App\Repositories\CouponRepository;
use App\Services\CampaignService;
use Carbon\Carbon;
use Faker\Generator;
use Faker\Provider\Text;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Tests\TestCase;

class CouponServiceTest extends TestCase
{
    private int $id;
    private array $data;

    private CouponRepository $couponRepository;
    /**
     * @var coupon
     */
    private coupon $model;

    protected function setUp(): void
    {
        parent::setUp();
        $this->id = 1;
        $this->data = [
            'name' => 'Test Coupon' . rand(1, 10000),
            'code' => Text::regexify('[A-Za-z0-9]{20}'),
            'activation_date' => '2021-12-01',
            'campaign_id' => 1
        ];

        $this->model = new coupon();
        $this->couponRepository = new couponRepository($this->model);
    }

    /**
     * @group service-coupons
     */
    public function testGetAction()
    {
        $couponService = new \App\Services\CouponService($this->couponRepository);

        $result = $couponService->getList();

        $this->assertEquals(200, $result['httpCode']);
    }

    /**
     * @group service-coupon
     */

    public function testIdAction(): void
    {
        $couponService = new \App\Services\CouponService($this->couponRepository);

        $result = $couponService->show($this->id);

        $this->assertEquals(200, $result['httpCode']);
    }

    /**
     * @group service-coupon
     */
    public function testAddSuccessAction(): void
    {
        $couponService = new \App\Services\CouponService($this->couponRepository);

        $result = $couponService->store(new CouponRequest($this->data));
        $this->assertEquals(200, $result['httpCode']);
    }

    /**
     * @group service-coupon
     */
    public function testUpdateSuccessAction(): void
    {
        $couponService = new \App\Services\CouponService($this->couponRepository);

        $result = $couponService->update(new CouponRequest($this->data), $this->id);

        $this->assertEquals(200, $result['httpCode']);
    }
}
