<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campaign extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'campaign';
    protected $fillable = [
        'name',
        'campaign_start',
        'campaign_end',
        'status_id'
    ];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function blog()
    {
        return $this->hasMany(Blog::class);
    }

    public function coupon()
    {
        return $this->hasMany(Coupon::class);
    }

    public function product()
    {
        return $this->hasMany(Product::class);
    }
}
