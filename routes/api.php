<?php

use App\Http\Controllers\CampaignController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StatusController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Route::get('/campaignlist', [CampaignController::class, 'index']);
    Route::get('/campaign/{campaignId}', [CampaignController::class, 'getCampaign']);
    Route::post('/campaign', [CampaignController::class, 'createCampaign']);
    Route::put('/campaign/{campaignId}', [CampaignController::class, 'updateCampaign']);
    Route::delete('/campaign/{campaignId}', [CampaignController::class, 'deleteCampaign']);

    Route::get('/approvedCampaign', [CampaignController::class, 'getApproved']);

    Route::post('/campaign/{campaignId}/createblog', [\App\Http\Controllers\BlogController::class, 'createBlog']);
    Route::put('/campaign/{campaignId}/blog/{blogId}', [\App\Http\Controllers\BlogController::class, 'updateBlog']);
    Route::get('/campaign/{campaignId}/blog/{blogId}', [\App\Http\Controllers\BlogController::class, 'getBlog']);
    Route::get('/blogs', [\App\Http\Controllers\BlogController::class, 'index']);
    Route::post('/createblog', [\App\Http\Controllers\BlogController::class, 'createBlog']);
    Route::put('/blog/{blogId}', [\App\Http\Controllers\BlogController::class, 'updateBlog']);
    Route::post('/blog/{blogId}', [\App\Http\Controllers\BlogController::class, 'getBlog']);
    Route::delete('/blog/{blogId}', [\App\Http\Controllers\BlogController::class, 'delete']);

    Route::post('/campaign/{campaignId}/createproduct', [\App\Http\Controllers\ProductController::class, 'createProduct']);
    Route::put('/campaign/{campaignId}/product/{productId}', [\App\Http\Controllers\ProductController::class, 'updateProduct']);
    Route::get('/campaign/{campaignId}/product/{productId}', [\App\Http\Controllers\ProductController::class, 'getProduct']);
    Route::get('/products', [\App\Http\Controllers\ProductController::class, 'index']);
    Route::post('/createproduct', [\App\Http\Controllers\ProductController::class, 'createProduct']);
    Route::put('/product/{productId}', [\App\Http\Controllers\ProductController::class, 'updateProduct']);
    Route::get('/product/{productId}', [\App\Http\Controllers\ProductController::class, 'getProduct']);
    Route::delete('/product/{productId}', [\App\Http\Controllers\ProductController::class, 'delete']);

    Route::post('/campaign/{campaignId}/createcoupon', [\App\Http\Controllers\CouponController::class, 'createCoupon']);
    Route::post('/campaign/{campaignId}/coupon/{couponId}', [\App\Http\Controllers\CouponController::class, 'updateCouponCampaign']);
    Route::get('/campaign/{campaignId}/coupon/{couponId}', [\App\Http\Controllers\CouponController::class, 'getCouponCampaign']);
    Route::post('/campaign/{campaignId}/coupon/{couponId}', [\App\Http\Controllers\CouponController::class, 'updateCoupon']);
    Route::put('/campaign/{campaignId}/product/{productId}/del', [\App\Http\Controllers\CouponController::class, 'deleteFromCampaign']);
    Route::get('/coupon', [\App\Http\Controllers\CouponController::class, 'index']);
    Route::post('/createcoupon', [\App\Http\Controllers\CouponController::class, 'createCoupon']);
    Route::put('/coupon/{couponId}', [\App\Http\Controllers\CouponController::class, 'updateCoupon']);
    Route::get('/coupon/{couponId}', [\App\Http\Controllers\CouponController::class, 'getCoupon']);
    Route::delete('/coupon/{couponId}', [\App\Http\Controllers\CouponController::class, 'delete']);
});
Route::group(['prefix' => 'client'], function () {
    Route::get('/campaigns', [CampaignController::class, 'getApproved']);
});



