@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">* Name:</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{$coupon->name}}" required/>
                    </div>
                    <div class="form-group">
                        <label for="code">* Code:</label>
                        <input type="text" class="form-control" id="code" name="code" value="{{$coupon->code}}"/>
                    </div>
                    <div class="form-group">
                        <label for="activation_date">* Activation date:</label>
                        {{--                        <input type="text"  class="form-control" id="activation_date" name="activation_date" required>--}}
                        <input type="date" class="form-control" id="activation_date" name="activation_date" value="{{$coupon->activation_date}}"
                               required/>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <input type="submit" value="Save" class="btn btn-primary mb-2"/>
                        <a href="{{url('/admin/campaign/'.$campaign)}}" class="btn btn-secondary mb-2">Back</a>
                    </div>
                </form>
                <form action="{{ url('/admin/campaign/{id}/coupon/{couponId}/del') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <input type="submit" name="delete" value="Delete"
                               class="btn btn-danger mb-2"><i class="fa fa-trash fa-xs"></i>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--    <script type="text/javascript">--}}
    {{--        $('#activation_date').datepicker({--}}
    {{--            beforeShowDay: $.datepicker.noWeekends--}}
    {{--        });--}}

    {{--    </script>--}}
@endsection
