<?php

namespace Services;

use App\Http\Requests\BlogRequest;
use App\Models\Blog;
use App\Repositories\BlogRepository;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class BlogServiceTest extends TestCase
{
    private int $id;
    private array $data;

    private BlogRepository $blogRepository;
    /**
     * @var blog
     */
    private blog $model;

    protected function setUp(): void
    {
        parent::setUp();

        $this->id = 1;
        $this->data = [
            'title' => 'Test Blog' . rand(1, 10000),
            'contents' => 'Test Contents',
            'publication_date' => '2021-12-01'
        ];

        $this->model = new blog();
        $this->blogRepository = new blogRepository($this->model);
    }

    /**
     * @group service-blog
     */
    public function testGetAction()
    {
        $blogService = new \App\Services\BlogService($this->blogRepository);

        $result = $blogService->getList();

        $this->assertEquals(200, $result['httpCode']);
    }

    /**
     * @group service-blog
     */

    public function testIdAction(): void
    {
        $blogService = new \App\Services\BlogService($this->blogRepository);

        $result = $blogService->show($this->id);

        $this->assertEquals(200, $result['httpCode']);
    }

    /**
     * @group service-blog
     */
    public function testAddSuccessAction(): void
    {
        $blogService = new \App\Services\BlogService($this->blogRepository);

        $result = $blogService->store(new BlogRequest($this->data));

        $this->assertEquals(200, $result['httpCode']);
    }

    /**
     * @group service-blog
     */
    public function testUpdateSuccessAction(): void
    {
        $blogService = new \App\Services\BlogService($this->blogRepository);

        $result = $blogService->update(new BlogRequest($this->data), $this->id);

        $this->assertEquals(200, $result['httpCode']);
    }
}
