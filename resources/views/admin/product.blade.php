@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">* Name:</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{$product->name}}" required/>
                    </div>
                    <div class="form-group">
                        <label for="publication_date">* Publication date:</label>
                        {{--                        <input type="text"  class="form-control" id="publication_date" name="publication_date" required>--}}
                        <input type="date" class="form-control" id="publication_date" name="publication_date" value="{{$product->publication_date}}"
                               required/>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <input type="submit" value="Save" class="btn btn-primary mb-2"/>
                        <a href="{{url('/admin/campaign/'.$campaign)}}" class="btn btn-secondary mb-2">Back</a>
                    </div>
                </form>
                <form action="{{ url('/admin/campaign/{id}/product/{productId}/del') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <input type="submit" name="delete" value="Delete"
                               class="btn btn-danger mb-2"><i class="fa fa-trash fa-xs"></i>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--    <script type="text/javascript">--}}
    {{--        $('#publication_date').datepicker({--}}
    {{--            beforeShowDay: $.datepicker.noWeekends--}}
    {{--        });--}}

    {{--    </script>--}}
@endsection
