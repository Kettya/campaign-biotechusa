<?php

namespace Repositories;

use App\Models\Coupon;
use Carbon\Carbon;
use Faker\Provider\Text;
use Tests\TestCase;

class CouponReporitoryTest extends TestCase
{
    private int $id;
    private array $data;

    /**
     * @var coupon
     */
    private coupon $model;

    protected function setUp(): void
    {
        parent::setUp();

        $this->id = 1;
        $this->data = [
            'name' => 'Test Coupon' . rand(1, 10000),
            'code' => Text::regexify('[A-Za-z0-9]{20}'),
            'activation_date' => '2021-12-01',
            'campaign_id' => 1
        ];

        $this->model = new coupon();
    }

    /**
     * @group Repository-coupon
     */
    public function testGetAllAction()
    {
        $couponRepository = new \App\Repositories\CouponRepository($this->model);

        $result = $couponRepository->getAll();

        $this->assertNotNull($result);
    }

    /**
     * @group Repository-coupon
     */

    public function testIdAction(): void
    {
        $couponRepository = new \App\Repositories\CouponRepository($this->model);

        $result = $couponRepository->getShow($this->id);

        $this->assertNotNull($result);
    }

    /**
     * @group Repository-coupon
     */
    public function testAddSuccessAction(): void
    {
        $couponRepository = new \App\Repositories\CouponRepository($this->model);

        $result = $couponRepository->persist(new Coupon($this->data));

        $this->assertNotNull($result);
    }
}
