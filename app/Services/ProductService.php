<?php

namespace App\Services;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Repositories\ProductRepository;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProductService
 * @package App\Services
 */
class ProductService
{
    private ProductRepository $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getList(): array
    {
        try {
            $products = $this->repository->getAll();
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }
        return ['message' => 'Get Coupon List Successfully', 'data' => $products, 'httpCode' => Response::HTTP_OK];
    }

    public function show(int $productId): array
    {
        try {
            $product = $this->repository->getShow($productId);
            if ($product === null) {
                return ['message' => 'Not found coupon', 'data' => null, 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => null, 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }
        return ['message' => 'Get Coupon Successfully', 'data' => $product, 'httpCode' => Response::HTTP_OK];
    }

    public function store(ProductRequest $request, int $campaignId = null): array
    {
        try {
            $campaign = !empty($campaignId) ? $campaignId : null;
            $product = new Product([
                'name' => $request->name,
                'publication_date' => $request->publication_date,
                'campaign_id' => $campaign
            ]);
            $this->repository->persist($product);
        } catch (\Exception $e) {
            return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }
        return ['message' => 'Product Created Successfully', 'data' => $product, 'httpCode' => Response::HTTP_OK];
    }

    public function update(ProductRequest $request, int $couponId): array
    {
        $product = $this->repository->getShow($couponId);

        if ($product) {
            try {
                $campaign = !is_null($request->campaign_id) ? $request->campaign_id : $product->campaign_id;
                $name = !is_null($request->name) ? $request->name : $product->name;
                $publication_date = !is_null($request->publication_date) ? $request->publication_date : $product->publication_date;

                $product->name = $name;
                $product->publication_date = $publication_date;
                $product->campaign_id = $campaign;

                $this->repository->persist($product);

            } catch (\Exception $e) {
                return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }
        } else {
            return ['message' => 'Error product update', 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }
        return ['message' => 'Product Updated Successfully', 'data' => $product, 'httpCode' => Response::HTTP_OK];
    }

    public function destroy(int $productId): array
    {
        $product = $this->repository->getShow($productId);

        if ($product) {
            try {
                $this->repository->destroy($product);
            } catch (\Exception $e) {
                return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }
        } else {
            return ['message' => 'Error coupon update', 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }
        return ['message' => 'Product Delete Successfully', 'data' => $product, 'httpCode' => Response::HTTP_OK];
    }

}
