<?php

namespace App\Http\Controllers;

use App\Http\Requests\CouponRequest;
use App\Models\Coupon;
use App\Rules\FirstOrLastTreeDay;
use App\Services\CouponService;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CouponController extends Controller
{
    /**
     * @var CouponService
     */
    private CouponService $service;

    public function __construct(CouponService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $result = $this->service->getList();
        return response($result['data'], $result['httpCode']);
    }

    public function createCoupon(CouponRequest $request)
    {
        $result = $this->service->store($request);

        return response($result['data'], $result['httpCode']);
    }

//    public function getCouponCampaign($couponId, $campaignId)
//    {
//        $coupon = Coupon::query()->where('id', $couponId)->where('campaign_id', $campaignId)->first();
//        return $coupon;
//
//    }

    public function getCoupon($couponId)
    {
        $result = $this->service->show($couponId);

        return response($result['data'], $result['httpCode']);

    }

//    public function updateCouponCampaign(Request $request, $campaignId, $couponId)
//    {
//        $request->validate([
//            'name' => ['required'],
//            'code' => ['required'],
//            'activation_date' => ['required', 'date', new FirstOrLastTreeDay]
//        ]);
//        $coupon = Coupon::query()->where('id', $couponId)->where('campaign_id', $campaignId)->first();
//        $name = !empty($request->name) ? $request->name : $coupon->name;
//        $code = !empty($request->code) ? $request->code : $coupon->code;
//        $activation_date = !empty($request->activation_date) ? $request->activation_date : $coupon->activation_date;
//        $coupon->update([
//            'name' => $name,
//            'code' => $code,
//            'activation_date' => $activation_date
//        ]);
//
//        return $coupon;
//
//    }

    public function updateCoupon(CouponRequest $request, $couponId)
    {
        $result = $this->service->update($request, $couponId);
        return response($result['data'], $result['httpCode']);

    }

//    public function deleteFromCampaign($campaignId, $couponId)
//    {
//        $coupon = Coupon::query()->where('id', $couponId)->where('campaign_id', $campaignId)->first();
//        $coupon->update([
//            'campaign_id' => null
//        ]);
//        return $coupon;
//    }

    public function delete($couponId)
    {
        $coupon = $this->service->destroy($couponId);
        return response($coupon);
    }
}
