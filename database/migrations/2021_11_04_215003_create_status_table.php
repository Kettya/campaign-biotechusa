<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        DB::table('status')->insert(

            array(
                ['name' => 'Created', 'created_at' => \Carbon\Carbon::now()],
                ['name' => 'In progress', 'created_at' => \Carbon\Carbon::now()],
                ['name' => 'Allowed', 'created_at' => \Carbon\Carbon::now()],
                ['name' => 'Denied', 'created_at' => \Carbon\Carbon::now()])

        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status');
    }
}
