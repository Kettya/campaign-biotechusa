<?php

namespace Services;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Repositories\ProductRepository;
use Carbon\Carbon;
use Tests\TestCase;

class ProductServiceTest extends TestCase
{
    private int $id;
    private array $data;

    private ProductRepository $productRepository;
    /**
     * @var product
     */
    private product $model;

    protected function setUp(): void
    {
        parent::setUp();

        $this->id = 1;
        $this->data = [
            'name' => 'Test Product' . rand(1, 10000),
            'publication_date' => Carbon::tomorrow()
        ];

        $this->model = new product();
        $this->productRepository = new productRepository($this->model);
    }

    /**
     * @group service-campaigns
     */
    public function testGetAction()
    {
        $productService = new \App\Services\ProductService($this->productRepository);

        $result = $productService->getList();

        $this->assertEquals(200, $result['httpCode']);
    }

    /**
     * @group service-campaigns
     */

    public function testIdAction(): void
    {
        $productService = new \App\Services\ProductService($this->productRepository);

        $result = $productService->show($this->id);

        $this->assertEquals(200, $result['httpCode']);
    }

    /**
     * @group service-campaigns
     */
    public function testAddSuccessAction(): void
    {
        $productService = new \App\Services\ProductService($this->productRepository);

        $result = $productService->store(new ProductRequest($this->data));

        $this->assertEquals(200, $result['httpCode']);
    }

    /**
     * @group service-campaigns
     */
    public function testUpdateSuccessAction(): void
    {
        $productService = new \App\Services\ProductService($this->productRepository);

        $result = $productService->update(new ProductRequest($this->data), $this->id);

        $this->assertEquals(200, $result['httpCode']);
    }
}
