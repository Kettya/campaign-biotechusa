<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository
{
    /**
     * @var Product
     */
    private Product $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $products = $this->model->get()->all();

        return $products;
    }

    /**
     * @param $productId
     * @return object
     */
    public function getShow($productId): object
    {
        return $this->model->query()->findOrFail($productId);
    }

    /**
     * @param object $data
     * @return object
     * @throws \Exception
     */
    public function persist(object $data): object
    {
        try {
            $data->save();

            return $data;

        } catch (\Exception $e) {

            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param object $product
     * @return int
     * @throws \Exception
     */
    public function destroy(object $product): int
    {
        try {
            return $product->delete();

        } catch (\Exception $e) {

            throw new \Exception($e->getMessage());
        }
    }
}
