<?php

namespace App\Repositories;

use App\Models\Coupon;

/**
 * Class couponRepository
 * @package App\Repository
 */
class CouponRepository
{
    /**
     * @var Coupon
     */
    private Coupon $model;

    /**
     * couponRepository constructor.
     * @param Coupon $model
     */
    public function __construct(Coupon $model)
    {
        $this->model = $model;
    }

    public function getAll(): array
    {
        $coupons = $this->model->get()->all();

        return $coupons ;
    }

    /**
     * @param $couponId
     * @return object
     */
    public function getShow($couponId): object
    {
        return $this->model->query()->findOrFail($couponId);
    }

    /**
     * @param object $data
     * @return coupon
     * @throws \Exception
     */
    public function persist(object $data): object
    {
        try {
            $data->save();

            return $data;

        } catch (\Exception $e) {

            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param object $coupon
     * @return int|null
     * @throws \Exception
     */
    public function destroy(object $coupon): int
    {
        try {
            return $coupon->delete();

        } catch (\Exception $e) {

            throw new \Exception($e->getMessage());
        }
    }
}
