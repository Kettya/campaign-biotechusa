<?php

namespace Tests\Unit;

use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_create_product()
    {
        $response = $this->call('POST', '/api/admin/campaign/1/createproduct', [
            'name' => 'First Product',
            'publication_date' => '2021-11-05'
        ]);
        $response->assertStatus($response->status(), 302);
    }

    public function test_create_product_without_campaign()
    {
        $response = $this->call('POST', '/api/admin/createproduct', [
            'name' => 'First Product without',
            'publication_date' => '2021-11-14'
        ]);
        $response->assertStatus($response->status(), 302);
    }

    public function test_create_product_wrong_without_campaign()
    {
        $response = $this->call('POST', '/api/admin/createproduct', [
            'name' => 'First Product without',
            'publication_date' => '2021-11-14asd'
        ]);
        $response->assertStatus($response->status(), 302);
    }

    public function test_create_product_invalid()
    {
        $response = $this->call('POST', '/api/admin/campaign/1/createproduct', [
            'name' => 'Second Product',
            'publication_date' => '2021-11-05aaaaaa'
        ]);
        $response->assertStatus($response->status(), 422);
    }

    public function test_update_product()
    {
        $response = $this->call('POST', '/api/admin/campaign/1/product/1', [
            'name' => 'First Product changed',
            'publication_date' => '2021-11-21'
        ]);
        $response->assertStatus($response->status(), 302);
    }

    public function test_update_product_add_campaign()
    {
        $response = $this->call('POST', '/api/admin/product/2', [
            'product_id' => 1
        ]);
        $response->assertStatus($response->status(), 302);
    }

    public function test_update_invalid_product()
    {
        $response = $this->call('POST', '/api/admin/campaign/1/product/1', [
            'name' => 'First Product changed',
            'publication_date' => '2021-11-21ssss'
        ]);
        $response->assertStatus($response->status(), 422);
    }
}
