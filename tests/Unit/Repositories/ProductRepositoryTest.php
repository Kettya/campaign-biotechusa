<?php

namespace Repositories;

use App\Models\Product;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class ProductRepositoryTest extends TestCase
{

    private int $id;
    private array $data;

    /**
     * @var product
     */
    private product $model;

    protected function setUp(): void
    {
        parent::setUp();

        $this->id = 1;
        $this->data = [
            'name' => 'Test Product' . rand(1, 10000),
            'publication_date' => Carbon::tomorrow()
        ];

        $this->model = new product();
    }

    /**
     * @group Repository-product
     */
    public function testGetAllAction()
    {
        $productRepository = new \App\Repositories\ProductRepository($this->model);

        $result = $productRepository->getAll();

        $this->assertNotNull($result);
    }

    /**
     * @group Repository-product
     */

    public function testIdAction(): void
    {
        $productRepository = new \App\Repositories\ProductRepository($this->model);

        $result = $productRepository->getShow($this->id);

        $this->assertNotNull($result);
    }

    /**
     * @group Repository-product
     */
    public function testAddSuccessAction(): void
    {
        $productRepository = new \App\Repositories\ProductRepository($this->model);

        $result = $productRepository->persist(new Product($this->data));

        $this->assertNotNull($result);
    }

}
