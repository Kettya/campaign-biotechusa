@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card radius">
                    <h3 class=" card-header">Campaigns  <a href="{{url('/admin/createcampaign')}}" class="btn btn-primary mb-2">Add new Campaign</a></h3>
                    <div class="card-block table-responsive">
                        <table class="table">
                            <tbody>
                            @foreach($campaings as $campaign)
                                <tr>
                                    <td><a class="nav-link" href="/admin/campaign/{{$campaign->id}}">{{ $campaign->name }}</a> </td>
                                    <td><a class="nav-link" href="/admin/campaign/{{$campaign->id}}">{{ $campaign->status->name }}</a> </td>
                                    <td><a class="nav-link" href="/admin/campaign/{{$campaign->id}}">{{ $campaign->campaign_start }}</a> </td>
                                    <td><a class="nav-link" href="/admin/campaign/{{$campaign->id}}">{{ $campaign->campaign_end }}</a> </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
