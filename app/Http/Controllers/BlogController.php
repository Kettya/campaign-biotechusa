<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogRequest;
use App\Models\Blog;
use App\Models\Product;
use App\Rules\MustBeWeekDay;
use App\Services\BlogService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{
    /**
     * @var BlogService
     */
    private BlogService $service;

    public function __construct(BlogService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $result = $this->service->getList();

        return response($result['data'], $result['httpCode']);
    }

    public function createBlog(BlogRequest $request, $campaignId = null)
    {
        $result = $this->service->store($request, $campaignId);

        return response($result['data'], $result['httpCode']);
    }

//    public function getBlogCampaign($blogId, $campaignId = null)
//    {
//        $blog = Blog::query()->where('id', $blogId)->where('campaign_id', $campaignId)->first();
//        return $blog;
//    }

    public function getBlog($blogId)
    {
        $result = $this->service->show($blogId);

        return response($result['data'], $result['httpCode']);
    }

//    public function updateBlogCampaign(Request $request, $blogId, $campaignId)
//    {
//        $request->validate([
//            'title' => ['required', 'unique:blogs'],
//            'contents' => ['required'],
//            'publication_date' => ['required', 'date', new MustBeWeekDay]
//        ]);
//        $blog = Blog::query()->where('id', $blogId)->where('campaign_id', $campaignId)->first();
//        $title = !empty($request->title) ? $request->title : $blog->title;
//        $contents = !empty($request->contents) ? $request->contents : $blog->contents;
//        $publication_date = !empty($request->publication_date) ? $request->publication_date : $blog->publication_date;
//        $blog->update([
//            'title' => $title,
//            'contents' => $contents,
//            'publication_date' => $publication_date
//        ]);
//
//        return $blog;
//
//    }

    public function updateBlog(BlogRequest $request, $blogId, $campaignId = null)
    {
        $result = $this->service->update($request, $blogId, $campaignId);

        return response($result['data'], $result['httpCode']);

    }

//    public function deleteFromCampaign($campaignId, $blogId)
//    {
//        $blog = Blog::query()->where('id', $blogId)->where('campaign_id', $campaignId)->first();
//        $blog->update([
//            'campaign_id' => null
//        ]);
//        return $blog;
//    }

    public function delete($productId)
    {
        $blog = $this->service->destroy($productId);

        return response($blog);
    }

}
