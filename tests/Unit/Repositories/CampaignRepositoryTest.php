<?php

namespace Tests\Unit\Repositories;

use App\Http\Requests\CampaignRequest;
use App\Models\Campaign;
use App\Repositories\CampaignRepository;
use Carbon\Carbon;
use Tests\TestCase;

class CampaignRepositoryTest extends TestCase
{
    private int $id;
    private array $data;

    /**
     * @var campaign
     */
    private campaign $model;

    protected function setUp(): void
    {
        parent::setUp();

        $this->id = 1;
        $this->data = [
            'name' => 'Test Campaign' . rand(1, 10000),
            'campaign_start' => '2021-11-07',
            'campaign_end' => Carbon::tomorrow(),
            'status_id' => 3
        ];

        $this->model = new campaign();
    }

    /**
     * @group Repository-campaigns
     */
    public function testGetAllAction()
    {
        $campaignsRepository = new \App\Repositories\CampaignRepository($this->model);

        $result = $campaignsRepository->getAll();

        $this->assertNotNull($result);
    }

    /**
     * @group Repository-campaigns
     */

    public function testIdAction(): void
    {
        $campaignsRepository = new \App\Repositories\CampaignRepository($this->model);

        $result = $campaignsRepository->getShow($this->id);

        $this->assertNotNull($result);
    }

    /**
     * @group Repository-campaigns
     */
    public function testAddSuccessAction(): void
    {
        $campaignsRepository = new \App\Repositories\CampaignRepository($this->model);

        $result = $campaignsRepository->persist(new Campaign($this->data));

        $this->assertNotNull($result);
    }
}
