@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">* Name:</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{$campaign->name}}"
                               required/>
                    </div>
                    <div class="form-group">
                        <label for="campaign_start">* Campaign Start:</label>
                        <input type="date" class="form-control" id="campaign_start"
                               value="{{ $campaign->campaign_start}}"
                               required/>
                    </div>
                    <div class="form-group">
                        <label for="campaign_end">* Campaign End: </label>
                        <input type="date" class="form-control" id="campaign_end" name="campaign_end"
                               value="{{$campaign->campaign_end}}"
                               required/>
                    </div>
                    <select name="status" id="status">
                        @foreach($statuses as $stat)
                            <option value="{{$stat->id}}"
                                    @if($stat->id==$status->id)selected @endif>{{$stat->name}} </option>
                        @endforeach
                    </select>
                    {{--                    <input type="text" class="date" id="datepicker">--}}
                    <div class="form-group">
                        <input type="submit" value="Save" class="btn btn-primary mb-2"/>
                        <a href="{{redirect()->back()}}" class="btn btn-secondary mb-2">Back</a>
                        <form action="{{ url('/admin/campaign/{id}/del') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <input type="submit" name="delete" value="Delete"
                                       class="btn btn-danger mb-2"><i class="fa fa-trash fa-xs"></i>
                            </div>
                        </form>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card radius">
                    <h3 class=" card-header">Blogs {{$campaign->id}}<a
                            href="{{url('/admin/campaign/'.$campaign->id.'/createblog')}}" class="btn btn-primary mb-2">Add
                            new Blog</a></h3>
                    <div class="card-block table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Publication date</th>
                                <th>Created at</th>
                                <th>Updated at</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($blogs as $blog)
                                <tr>
                                    <td><a class="nav-link"  href="/admin/campaign/{{$campaign->id}}/blog/{{$blog->id}}">{{ $blog->title }}</a>
                                    </td>
                                    <td><a class="nav-link"
                                           href="/admin/campaign/{{$campaign->id}}/blog/{{$blog->id}}">{{ $blog->publication_date }}</a></td>
                                    <td><a class="nav-link"
                                           href="/admin/campaign/{{$campaign->id}}/blog/{{$blog->id}}">{{ $blog->created_at }}</a></td>
                                    <td><a class="nav-link"
                                           href="/admin/campaign/{{$campaign->id}}/blog/{{$blog->id}}">{{ $blog->updated_at }}</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card radius">
                    <h3 class=" card-header">Products {{$campaign->id}}<a
                            href="{{url('/admin/campaign/'.$campaign->id.'/createproduct')}}"
                            class="btn btn-primary mb-2">Add new Product</a></h3>
                    <div class="card-block table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Publication date</th>
                                <th>Created at</th>
                                <th>Updated at</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td><a class="nav-link"
                                           href="/admin/campaign/{{$campaign->id}}/product/{{$product->id}}">{{ $product->name }}</a></td>
                                    <td><a class="nav-link"
                                           href="/admin/campaign/{{$campaign->id}}/product/{{$product->id}}">{{ $product->publication_date }}</a>
                                    </td>
                                    <td><a class="nav-link"
                                           href="/admin/campaign/{{$campaign->id}}/product/{{$product->id}}">{{ $product->created_at }}</a></td>
                                    <td><a class="nav-link"
                                           href="/admin/campaign/{{$campaign->id}}/product/{{$product->id}}">{{ $product->updated_at }}</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card radius">
                    <h3 class=" card-header">Coupons {{$campaign->id}}<a
                            href="{{url('/admin/campaign/'.$campaign->id.'/createcoupon')}}"
                            class="btn btn-primary mb-2">Add new Coupons</a></h3>
                    <div class="card-block table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Publication date</th>
                                <th>Code</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($coupons as $coupon)
                                <tr>
                                    <td><a class="nav-link"
                                           href="/admin/campaign/{{$campaign->id}}/coupon/{{$coupon->id}}">{{ $coupon->name }}</a></td>
                                    <td><a class="nav-link"
                                           href="/admin/campaign/{{$campaign->id}}/coupon/{{$coupon->id}}">{{ $coupon->activation_date }}</a>
                                    </td>
                                    <td><a class="nav-link"
                                           href="/admin/campaign/{{$campaign->id}}/coupon/{{$coupon->id}}">{{ $coupon->code }}</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        // var array = ["06-11-2021","02-12-2021","23-11-2021","17-11-2021"];
        $('#datepicker').datepicker({
            // beforeShowDay: function(date){
            //     var string = jQuery.datepicker.formatDate('dd-mm-yy', date);
            //     return [ array.indexOf(string) === -1 ]
            // }
            beforeShowDay: $.datepicker.noWeekends
        });

    </script>
@endsection
