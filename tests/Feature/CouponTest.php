<?php

namespace Tests\Unit;

use Tests\TestCase;

class CouponTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_create_coupon_with_good_date_first()
    {
        $response = $this->call('POST', '/api/admin/campaign/1/createcoupon', [
            'name' => 'First Coupon',
            'code'=>'q1w2e3r4t5',
            'activation_date' => '2021-11-03'
        ]);
        $response->assertStatus($response->status(),302);
    }

    public function test_create_coupon_with_good_date_last()
    {
        $response = $this->call('POST', '/api/admin/campaign/1/createcoupon', [
            'name' => 'Second Coupon',
            'code'=>'q1w2e3r4t5z66',
            'activation_date' => '2021-11-28'
        ]);
        $response->assertStatus($response->status(),302);
    }

    public function test_create_coupon_with_good_date_first_without_campaign()
    {
        $response = $this->call('POST', '/api/admin/createcoupon', [
            'name' => 'First Coupon whitouth campaign',
            'code'=>'q1w2e3r4t5Without',
            'activation_date' => '2021-12-03'
        ]);
        $response->assertStatus($response->status(),302);
    }

    public function test_create_coupon_with_good_date_last_without_campaign()
    {
        $response = $this->call('POST', '/api/admin/createcoupon', [
            'name' => 'Second Coupon without campaign',
            'code'=>'q1w2e3r4t5z66Without',
            'activation_date' => '2021-11-28'
        ]);
        $response->assertStatus($response->status(),302);
    }

    public function test_create_coupon_with_wrong_date_first()
    {
        $response = $this->call('POST', '/api/admin/campaign/1/createcoupon', [
            'name' => 'Wrong Coupon',
            'code'=>'q1w2e3r4t5wrong',
            'activation_date' => '2021-11-04'
        ]);
        $response->assertStatus($response->status(),422);
    }

    public function test_create_coupon_with_wrong_date_last()
    {
        $response = $this->call('POST', '/api/admin/campaign/1/createcoupon', [
            'name' => 'Wrong Coupon2',
            'code'=>'q1w2e3r4t5wrong2',
            'activation_date' => '2021-11-27'
        ]);
        $response->assertStatus($response->status(),422);
    }

    public function test_create_coupon_with_wrong_date_first_without_campaign()
    {
        $response = $this->call('POST', '/api/admin/createcoupon', [
            'name' => 'Wrong Coupon without',
            'code'=>'q1w2e3r4t5wrongwithout',
            'activation_date' => '2021-11-04'
        ]);
        $response->assertStatus($response->status(),422);
    }

    public function test_create_coupon_with_wrong_date_last_without_campaign()
    {
        $response = $this->call('POST', '/api/admin/createcoupon', [
            'name' => 'Wrong Coupon2 whithout',
            'code'=>'q1w2e3r4t5wrong2without',
            'activation_date' => '2021-11-27'
        ]);
        $response->assertStatus($response->status(),422);
    }

    public function test_update_coupon_with_wrong_date()
    {
        $response = $this->call('POST', '/api/admin/campaign/2/coupon/1', [
            'name' => 'Wrong Coupon updated',
            'code'=>'q1w2e3r4t5wrong1',
            'activation_date' => '2021-11-11'
        ]);
        $response->assertStatus($response->status(),422);
    }
    public function test_update_add_campaign()
    {
        $response = $this->call('POST', '/api/admin/coupon/3', [
            'campaign_id' => '1',
        ]);
        $response->assertStatus($response->status(),302);
    }

    public function test_update_coupon_with_valid_date()
    {
        $response = $this->call('POST', '/api/admin/campaign/2/coupon/2', [
            'name' => 'Second updated valid',
            'code'=>'q1w2e3r4t5',
            'activation_date' => '2022-02-01'
        ]);
        $response->assertStatus($response->status(),302);
    }
}
